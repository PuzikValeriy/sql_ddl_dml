﻿/*CREATE TABLE addresses(
	id BIGINT NOT NULL,
	city TEXT NOT NULL,
	region TEXT NOT NULL,
	postal_code VARCHAR(5) NOT NULL,
	street TEXT NOT NULL,
	house_number BIGINT NOT NULL,
	flat_number BIGINT
	);*/
	
/*ALTER TABLE addresses 
	ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);*/
	
/*CREATE TABLE users(
	id BIGINT PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	birthdate DATE NOT NULL
	);*/

/*ALTER TABLE users 
	ADD COLUMN address_id BIGINT NOT NULL;*/
	
/*ALTER TABLE users 
	ADD CONSTRAINT fk_users_address_id_addresses_id
	FOREIGN KEY (address_id) REFERENCES addresses (id);*/

/*CREATE TABLE currency(
	id BIGINT PRIMARY KEY,
	currency VARCHAR(3) UNIQUE
	);*/

/*CREATE TABLE accounts(
	id BIGINT PRIMARY KEY,
	balance MONEY,
	creation_date TIMESTAMP NOT NULL,
	user_id BIGINT NOT NULL REFERENCES users(id),
	currency_id BIGINT NOT NULL REFERENCES currency(id) 
	);*/

/*ALTER TABLE accounts 
	ADD COLUMN blocked BOOLEAN DEFAULT false;*/

/*INSERT INTO addresses VALUES(1,'Dnipro','AND','46378','Pravda',63,24);
INSERT INTO addresses VALUES(2,'Kyev','Kreshyatik','58964','Moskovskaya',23,81);
INSERT INTO addresses VALUES(3,'Dnipro','Jovtneviy','50764','Pobeda 6',15,67);
INSERT INTO addresses VALUES(4,'Dnipro','Babushkinskiy','84632','Topol 2',71,17);
INSERT INTO addresses VALUES(5,'Lviv','Shevchenkovskiy','65034','Shevchenko',33,90);
INSERT INTO addresses VALUES(6,'Dnipro','Babushkinskiy','84638','Topol3',43,16);*/

/*INSERT INTO users VALUES(1,'Dmitriy','Petrov','1985-10-12',1);
INSERT INTO users VALUES(2,'Vasiliy','Sidorov','1976-02-28',2);
INSERT INTO users VALUES(3,'Roman','Ivanov','1990-09-06',3);
INSERT INTO users VALUES(4,'Ivan','Romanovskiy','1981-08-18',4);
INSERT INTO users VALUES(5,'Sergey','Prohorov','1987-09-12',5);
INSERT INTO users VALUES(6,'Roman','Babkin','1978-10-22',6);*/

/*INSERT INTO currency VALUES(1,'USD');
INSERT INTO currency VALUES(2,'UAH');
INSERT INTO currency VALUES(3,'EUR');*/

INSERT INTO accounts VALUES(1,'149,37','2016-10-27 02:00:00',1,2,false);
INSERT INTO accounts VALUES(2,'2789,80','2016-11-02 02:00:00',2,1,false);
INSERT INTO accounts VALUES(3,'1795,10','2016-11-04 02:00:00',3,1,false);
INSERT INTO accounts VALUES(4,'73,65','2016-11-10 02:00:00',4,3,false);
INSERT INTO accounts VALUES(5,'856,23','2016-11-13 02:00:00',5,3,false);
INSERT INTO accounts VALUES(6,'6534,19','2016-11-16 02:00:00',6,1,false);

/*UPDATE accounts SET blocked = true WHERE id = 1;*/



